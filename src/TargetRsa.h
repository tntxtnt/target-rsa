#ifndef TT_TARGETRSA_H
#define TT_TARGETRSA_H

#include <iostream>
#include <cryptopp/rsa.h>
#include <cryptopp/modarith.h>
#include <cryptopp/nbtheory.h>
#include <cryptopp/algparam.h>
using namespace CryptoPP;

namespace TargetRsa
{
constexpr unsigned int KEYBITS = 4096; //4096 bit key only
constexpr unsigned int EXPONENT = 65537;
extern const Integer PMIN;
extern const Integer PMAX;

RSA::PrivateKey generate(const Integer&, RandomNumberGenerator&);

std::pair<bool, RSA::PrivateKey> 
generate1(const Integer&, const Integer&, RandomNumberGenerator&);

bool isCompliantWithFIPS_186_4(const RSA::PrivateKey&);
}

#endif