#include "pem.h"

std::string base64Encoding(const CryptoPP::RSA::PrivateKey& key)
{
    std::string output;
    CryptoPP::Base64Encoder encoder(
        new CryptoPP::StringSink(output),
        true,
        PEM_MAX_CHAR_PER_LINE);
    key.DEREncodePrivateKey(encoder);
    encoder.MessageEnd();
    return output;
}

void PEM_Save(const CryptoPP::RSA::PrivateKey& key, std::ostream& out)
{
    out << RSA_PRIV_BEG_LINE << std::endl
        << base64Encoding(key)
        << RSA_PRIV_END_LINE << std::endl;
}

void PEM_Save(const CryptoPP::RSA::PrivateKey& key, const std::string& filename)
{
    std::ofstream fout(filename.c_str());
    PEM_Save(key, fout);
}