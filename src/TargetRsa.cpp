#include "TargetRsa.h"

namespace TargetRsa
{
const Integer PMIN = Integer(3037000500u) << (KEYBITS/2 - 32);
const Integer PMAX = Integer::Power2(KEYBITS/2) - 1;

RSA::PrivateKey generate(const Integer& targetModulusStartingSequence,
                         RandomNumberGenerator& rng)
{
    Integer nmin = targetModulusStartingSequence;
    Integer nmax = targetModulusStartingSequence + 1;
    nmin <<= KEYBITS - nmin.BitCount();
    nmax <<= KEYBITS - nmax.BitCount();
    --nmax;

    std::cout << "Hit CTRL+C to stop\n";
    for (;;)
    {
        std::cout << ".";
        auto gen = generate1(nmin, nmax, rng);
        if (!gen.first) continue;
        auto key = std::move(gen.second);
        auto const& n = key.GetModulus();
        Integer beg = 0;
        for (unsigned i = 0; i < targetModulusStartingSequence.ByteCount(); ++i)
            beg = (beg<<8) + n.GetByte(KEYBITS/8 - i - 1);
        if (beg == targetModulusStartingSequence)
        {
            std::cout << "\n";
            return key;
        }
    }
}

std::pair<bool, RSA::PrivateKey> generate1(const Integer& nmin, const Integer& nmax,
                                           RandomNumberGenerator& rng)
{
    Integer p, q, e, n, d;
    e = Integer(EXPONENT);

    class RSAPrimeSelector : public PrimeSelector
    {
    public:
        RSAPrimeSelector(const Integer& e) : e{e} {}
        bool IsAcceptable(const Integer& candidate)const
        { return RelativelyPrime(e, candidate - Integer::One()); }
    private:
        Integer e;
    } selector(e);

    auto pParam = MakeParameters("RandomNumberType", Integer::PRIME)
        ("Min", PMIN)("Max", PMAX)
        (Name::PointerToPrimeSelector(), selector.GetSelectorPointer());
    p.GenerateRandom(rng, pParam);

    //Check if we can make params for q
    Integer qmin = nmin / p;
    Integer qmax = nmax / p;
    if (qmin < PMIN || qmax > PMAX || qmin >= PMAX || qmax <= PMIN) //we can't
        return {false, {}};

    auto qParam = MakeParameters("RandomNumberType", Integer::PRIME)
        ("Min", qmin)("Max", qmax)
        (Name::PointerToPrimeSelector(), selector.GetSelectorPointer());
    q.GenerateRandom(rng, qParam);

    //Check if (p-1, e) and (q-1, e) are coprime
    //Since e is prime, we only need to check p-1 % e == 0
    if ((p - 1) % e == 0 || (q - 1) % e == 0)
        return {false, {}};

    d = e.InverseMod(LCM(p - 1, q - 1));
    CRYPTOPP_ASSERT(d.IsPositive());
    n = p * q;

    RSA::PrivateKey key;
    key.Initialize(n, e, d);
    return {true, key};
}

bool isCompliantWithFIPS_186_4(const RSA::PrivateKey& key)
{
    // 1b
    auto const& e = key.GetPublicExponent();
    if (e.IsEven() || e <= Integer::Power2(16) || e >= Integer::Power2(256))
        return false;
    // 2a
    auto const& p = key.GetPrime1();
    auto const& q = key.GetPrime2();
    if (Integer::Gcd(p - 1, e) != 1 || Integer::Gcd(p - 1, e) != 1)
        return false;
    // 2b, 2c
    Integer lowerBound = Integer("3037000500") << (KEYBITS/2 - 32);
    Integer upperBound = Integer::Power2(KEYBITS/2) - 1;
    if (p < lowerBound || p > upperBound  ||  q < lowerBound || q > upperBound)
        return false;
    // 2d
    if ((p - q).AbsoluteValue() <= Integer::Power2(KEYBITS/2 - 100))
        return false;
    // 3a
    auto const& d = key.GetPrivateExponent ();
    if (d <= Integer::Power2(KEYBITS/2) || d >= LCM(p - 1, q - 1))
        return false;
    // 3b
    if (e * d % LCM(p - 1, q - 1) != 1)
        return false;

    return true;
}
}