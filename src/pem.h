#ifndef TT_PEM_H
#define TT_PEM_H

#include <cryptopp/rsa.h>
#include <cryptopp/base64.h>
#include <cryptopp/files.h>
#include <string>
#include <fstream>
#include <ostream>

const std::string RSA_PRIV_BEG_LINE = "-----BEGIN RSA PRIVATE KEY-----";
const std::string RSA_PRIV_END_LINE = "-----END RSA PRIVATE KEY-----";
const int PEM_MAX_CHAR_PER_LINE = 64;

std::string base64Encoding(const CryptoPP::RSA::PrivateKey&);
void PEM_Save(const CryptoPP::RSA::PrivateKey&, const std::string&);
void PEM_Save(const CryptoPP::RSA::PrivateKey&, std::ostream&);

#endif