#include <iostream>
#include <iomanip>
#include <cryptopp/osrng.h>
#include "TargetRsa.h"
#include "pem.h"
#include <sstream>
using namespace CryptoPP;

std::string getPemFileName(const RSA::PrivateKey&, unsigned int=16);

int main(int argc, char** argv)
{
    std::string begByteSeq;
    if (argc == 2) begByteSeq = argv[1];
    else
    {
        std::cout << "Beginning byte sequence (e.g. 0x888888): ";
        std::cin >> begByteSeq;
    }
    
    AutoSeededX917RNG<AES> rng;
    auto privKey = TargetRsa::generate(Integer(begByteSeq.c_str()), rng);
    std::cout << std::hex << privKey.GetModulus() << "\n";
    if (TargetRsa::isCompliantWithFIPS_186_4(privKey))
    {
        std::string filename = getPemFileName(privKey);
        std::cout << "Saving to '" << filename << "'... ";
        PEM_Save(privKey, filename);
        std::cout << "Done!\n";
    }
    else
    {
        std::cout << "privkey is not compliant with FIPS 186-4. Try again.\n";
    }
}

std::string getPemFileName(const RSA::PrivateKey& key, unsigned int byteCount)
{
    std::ostringstream oss;
    auto const& n = key.GetModulus();
    unsigned int keybits = n.BitCount();
    oss << std::hex << std::setfill('0');
    for (unsigned int i = 1; i < byteCount; ++i)
        oss << std::setw(2) << (int)n.GetByte(keybits/8 - i) << "-";
    oss << std::setw(2) << (int)n.GetByte(keybits/8 - byteCount);
    return oss.str() + ".pem";
}
