# Target RSA

## What
Generate 4096-bit RSA key which modulus has specific most significant bits (e.g `0x88888888...`, `0xbd20170630...`)

## How
You'll need a modern C++11 compiler, `CryptoPP` library, and GNU `make` to compile. Simply run `make run` to compile and generate your keys. Generated keys will be automatically save to a PEM format file.

Sample run (this is a throwaway key, don's show prime p or prime q to anyone!)

![Sample run](ss/sample1.png)

## Why
With the help of Let's Encrypt `certbot`, you can make the generated key your little blog's HTTPS certificate, to serve as a hidden Easter Egg. It would be fun if your certificate modulus started with your/its birthday's number: `0xbd20170630`

For example, we want to make generated private key (gpk) `aa-aa-aa-aa.pem` our `test.tritran.xyz` certificate:

* Switch to root: `su`
* Create a CSR from gpk: `openssl req -new -sha256 -key aa-aa-aa-aa.key -nodes -outform pem -out test.tritran.xyz.csr`
* Use Let's Encrypt to obtain a certificate: `certbot certonly -a manual -d test.tritran.xyz --csr test.tritran.xyz.csr`

Let's see some [maaaaaaaaagic](https://test.tritran.xyz):

![Sample cert](ss/sample2.png)

**Use at your own risk!** Second prime `q`'s random value range is limited by first prime `p`'s value so maaaybe it is not secured enough, but who hacks a personal blog anyway.
