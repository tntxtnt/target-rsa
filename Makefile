CXX=g++ -std=c++1z
RM=rm -rf
MKDIR=mkdir -p
ECHO=echo
rwildcard=$(wildcard $1$2) $(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2))

INCDIR=-Isrc
LNKDIR=

CFLAGS=${CPPFLAGS} -O2 -Wall
LFLAGS=${LDFLAGS} -s -lcryptopp

SRCDIR=src
SRCS=$(call rwildcard,$(SRCDIR),*.cpp)
OBJDIR=obj
OBJS=$(SRCS:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)
DEPDIR=dep
DEPS=$(OBJS:$(OBJDIR)/%.o=$(DEPDIR)/%.dep)
OBJDIRS=$(sort $(dir $(OBJS)))
DEPDIRS=$(OBJDIRS:$(OBJDIR)/%=$(DEPDIR)/%)
TGTDIRS=bin
MAKDIRS=
TGTNAME=target-rsa
TARGET=$(TGTDIRS)/$(TGTNAME)
RES=$(OBJDIR)/targetrsa.res

print-%  : ; @echo $* = $($*)

.PHONY: all run clean win

all: $(TARGET)

run: $(TARGET)
	./$(TGTDIRS)/$(TGTNAME)
	
clean:
	@$(RM) $(OBJDIR)
	@$(RM) $(DEPDIR)
	@$(RM) $(TARGET)
	
win: $(OBJS) $(RES) | $(TGTDIRS)
	$(CXX) $^ $(LNKDIR) -lmingw32 $(LFLAGS) -mwindows -o $(TARGET)
	
$(OBJDIRS):
	@$(MKDIR) $@
$(DEPDIRS):
	@$(MKDIR) $@
$(TGTDIRS):
	@$(MKDIR) $@

$(DEPDIR)/%.dep: $(SRCDIR)/%.cpp | $(DEPDIRS)
	$(eval OBJDEP=$(shell $(CXX) $(INCDIR) -MM $< -MT $(<:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)))
	$(eval DEPDEP=$(subst .o:,.dep:,$(OBJDEP:$(OBJDIR)/%=$(DEPDIR)/%)))
	@$(ECHO) $(OBJDEP) > $@
	@$(ECHO) $(DEPDEP) >> $@
-include $(DEPS)

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp | $(OBJDIRS)
	$(CXX) -c $(INCDIR) $(CFLAGS) $< -o $@
	
$(TARGET): $(OBJS) | $(TGTDIRS)
	$(CXX) $^ $(LNKDIR) $(LFLAGS) -o $(TARGET)
	
$(OBJDIR)/bday.res: $(SRCDIR)/bday.rc
	windres $^ -o $@ -O coff
